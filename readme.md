# Christopher Stokes
##### christopherstokes@gmail.com
##### (727) 710 - 1533
##### https://github.com/xkfngs
### Overview
---
#### Development Skills
* HTML/CSS 
* Javascript
* Node.js
* ASP
* PHP

#### OS Troubleshooting
* Microsoft Windows
* Linux
* Mac OSX

### Experience
---
#### Dealer Spike, LLC (2014 - Present)
##### Support Agent/Web Development/Email Administration
* Assist clients and fellow DS employees with the intricacies of proprietary CMS
* Change existing websites' content and styles to suit client needs
* Write custom code to add functionality per clients' needs/desires
* Work within a large legacy codebase and suggest changes to senior developers based on extensive QA
* Work in fast-paced environment with new challenges every day

#### Northwest Urological Clinic (2014)
##### Insurance Verification Coordinator
* Created organizational systems to meet demands from managing partners on insurance verification
* Extensive use of web-based medical software and utilities regarding insurance systems
* Provided excellent customer service for patients and eased the workload of collections by making patients aware of monetary
requirements before they go to the billing/collections stage

#### Dr. Mark Isenberg, PA (2008-2014)
##### Medical Scribe/Assistant/Tech Support/Software Migration Manager
* Oversaw two complete software migrations from a local database based medical records system to a cloud system and then to yet
another more fully-featured cloud system
* Worked in medical scribe capacity and provided support for both doctors and patients including phone support as well as in-person
support
* Taught the doctors as well as incoming co-workers the various medical records systems as practice expanded
* Optimized housecall routes for increased efficiency and shorter work hours
Worked in an extremely fast-paced and unique mobile practice with a high demand for flexibility

#### OrderCounter.com (2012)
##### Help Desk Tech Support/Web Development Intern
* Provided excellent customer service and technical support to restaurant owning clients with low tech knowledge
* Built Facebook and Google integration into website templates for ease of use for customer as well as for marketing purposes
* Built a prototype interactive front-end for manipulating MySQL databases and Microsoft SQL databases contemporaneously for the
purposes of user-created menus for tablet point-of-service system written for android interfacing with a vb.net desktop point-ofservice
system
* Learned new things quickly in a startup environment

#### University of West Florida (2003-2005)
##### Help Desk Tech Support/Computer Lab Tech
* Provided complete technical support for students and professors with any issues they had whether they be related to school software
or their own personal computers
* Deployed software to an entire computer lab of 100+ computers in a variety of fashions including over-network methods as well as
flashing with physical media
* Expanded troubleshooting abilities through working with a variety of clients and a plethora of issues that could take any possible form.

### References
---
#####Dr. Mark Isenberg
###### Dr. Mark Isenberg, PA
(727) 639-1973

<br />

#####Jason Calloway
######Dealer Spike, LLC
(360) 771-7907

<br />

#####Kris Wernowsky
######Freelance Reporter
(309) 255-1218
